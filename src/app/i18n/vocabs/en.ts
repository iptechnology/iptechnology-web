export const locale = {
  lang: 'en',
  data: {
    MENU: {
      MENU: 'Menu',
      HOME: 'Home',
      STEGENSEK: 'Stegensek Table',
      IPTECH_AGENTS: 'IPTech Agents',
    },
    HOME: {
      MAIN_TEXT_1:
        'You are entering a business project in an incipient development phase, sponsored by a large group of professionals specialized in various technical disciplines, motivated by a common objective: “<i> <u> <strong> Reduce the workplace accident rate </strong> </u>, ensuring regulatory and legislative compliance with regard to <strong> Personal Protective Equipment, PPE </strong> (hereinafter <strong> IP </strong>) regardless of the country in which they are manufactured, distributed, needed and/or used”.</i>',
      MAIN_TEXT_2:
        'The <strong> IPTechnology </strong> team wants to thank you for helping, on a voluntary basis, to debug the errors that, as in any other computer application in the launch phase, must be corrected. You are not only contributing to that laudable objective that we propose, but you will also be able, through the person who has invited you to participate, to be aware of our business successes and internationalization and, why not, to count on you in sometime, joining either as an independent professional, small investor, or simply having access to our tools for free.',
      MAIN_TEXT_3:
        '<strong> IPTechnology </strong> applies Information and Communication Technologies (ICT) to the world of IP, developing computer applications around the specific technology known as <strong> efpIP </strong> that facilitates their search, selection, choice. and traceability. IPTechnology is committed to maintaining a <strong> neutral and equidistant position </strong> with respect to the different agents that currently make up the IP market.',
      IP_RESEARCH: 'IPResearch',
      DOWNLOAD: 'Download our application:',
      IPT_R_TEXT:
        "The use, maintenance and conservation of PPE must be carried out in accordance with the manufacturer's instructions. The PPE to be used in each job corresponding to an organization will be those determined by its corresponding evaluation of occupational risks based on the tasks performed, the characteristics of the workplace, the equipment and machines used and the characteristics of the worker.",
    },
    STEGENSEK: {
      INFO: 'The <strong>Stegensek table</strong>, a living document constantly updated by the <strong>IPTechnology</strong> organization, is an elementary and organized list of Individual Protectors, <strong>IP</strong>, from its abbreviation in english <strong>Individual Protector</strong>, of those existing on the market, initially categorized by <strong>Families</strong>, according to the area of the body they protect or their general typology; and subsequently, by <strong>Labels/Tags</strong>, which specify the particular characteristics that distinguish some IP from others, within each of the <strong>Families</strong>. These characteristics may refer to structural, design, constructive, functional or normative requirements.',
      FAMILY: 'Family',
      TAG: 'Tag',
      IP: 'IP',
      SEARCH_FAMILY: 'Search family...',
      FAMILY_NOT_FOUND: 'Family not found',
      SEARCH_TAG: 'Search tag...',
      TAG_NOT_FOUND: 'Tag not found',
      IP_LIST: 'IP List',
      EMPTY_LIST: 'No IPs found under this family and tag.',
      REGULATORY_FRAMEWORK: 'Regulatory framework',
      REGULATORY_FRAMEWORK_LIST: {
        EUROPE: 'Europe',
      },
    },
    AGENTS: {
      COUNTRY: 'Country',
      COMUNITY: 'Comunity',
      CITY: 'City',
    },
    PAGINATOR: {
      PREVIOUS: 'Previous',
      NEXT: 'Next',
    },
  },
};
