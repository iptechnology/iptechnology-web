import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { BaseResponse } from 'src/app/shared/models/base-response';
import { FakeLoginService } from './fake-login.service';

@Component({
  selector: 'app-fake-login',
  templateUrl: './fake-login.component.html',
  styleUrls: ['./fake-login.component.scss']
})
export class FakeLoginComponent implements OnInit, OnDestroy {
  formGroup: FormGroup;

  private subscriptions$ = new Subscription();

  constructor(
    private fb: FormBuilder,
    private fakeLoginService: FakeLoginService
  ) { }

  ngOnDestroy(): void {
    this.subscriptions$?.unsubscribe();
  }

  ngOnInit(): void {
    this.loadForm();
  }

  loadForm(): void {
    this.formGroup = this.fb.group({
      password: new FormControl(null, Validators.required)
    });
  }

  login(): void {
    if (this.formGroup.valid) {
      const password = this.formGroup.get('password').value;

      // if (password === 'lavidaesbella' || +password >= 5000 && +password <= 5500) {
      //   this.fakeLoginService.logged.next(true);
      // }

      this.subscriptions$.add(
        this.fakeLoginService.login(password).subscribe((responseData: BaseResponse<boolean>) => {
          console.log(responseData);
          if (responseData.result) {
            this.fakeLoginService.logged.next(true);
          }
        })
      );
    }
  }

}
