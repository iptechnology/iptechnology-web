export const locale = {
  lang: 'de',
  data: {
    MENU: {
      MENU: 'Menü',
      HOME: 'Start',
      STEGENSEK: 'Tabelle Stegensek',
      IPTECH_AGENTS: 'Agenten IPTech',
    },
    HOME: {
      MAIN_TEXT_1:
        'Sie treten in einer beginnenden Entwicklungsphase in ein Geschäftsprojekt ein, das von einer großen Gruppe von Fachleuten gesponsert wird, die auf verschiedene technische Disziplinen spezialisiert sind und von einem gemeinsamen Ziel motiviert sind: "<i> <u> <strong> Reduzieren Sie die Unfallrate am Arbeitsplatz </strong> </u> Gewährleistung der Einhaltung gesetzlicher und gesetzlicher Bestimmungen in Bezug auf <strong> persönliche Schutzausrüstung, PSA </strong> (im Folgenden <strong> IP </strong>), unabhängig davon, in welchem ​​Land sie hergestellt, vertrieben, benötigt und/oder hergestellt werden verwendet”.</i>',
      MAIN_TEXT_2:
        'Das <strong> IPTechnology </strong> -Team möchte sich bei Ihnen dafür bedanken, dass Sie auf freiwilliger Basis beim Debuggen der Fehler geholfen haben, die wie bei jeder anderen Computeranwendung in der Startphase korrigiert werden müssen. Sie tragen nicht nur zu diesem lobenswerten Ziel bei, das wir vorschlagen, sondern Sie können durch die Person, die Sie zur Teilnahme eingeladen hat, auch über unsere Geschäftserfolge und Internationalisierung informiert werden und, warum nicht, irgendwann auf Sie zählen Sie können entweder als unabhängiger professioneller Kleininvestor beitreten oder einfach kostenlos auf unsere Tools zugreifen.',
      MAIN_TEXT_3:
        '<strong> IPTechnology </strong> wendet Informations- und Kommunikationstechnologien (IKT) auf die Welt des geistigen Eigentums an und entwickelt Computeranwendungen rund um die als <strong> efpIP </strong> bekannte Technologie, die die Suche, Auswahl, Auswahl und Rückverfolgbarkeit erleichtert . IPTechnology ist bestrebt, eine <strong> neutrale und äquidistante Position </strong> in Bezug auf die verschiedenen Agenten, aus denen sich der IP-Markt derzeit zusammensetzt, aufrechtzuerhalten.',
      IP_RESEARCH: 'IPResearch',
      DOWNLOAD: 'Laden Sie unsere Anwendung herunter:',
      IPT_R_TEXT:
        'Die Verwendung, Wartung und Konservierung von PSA muss gemäß den Anweisungen des Herstellers erfolgen. Die PSA, die in jedem einer Organisation entsprechenden Arbeitsplatz verwendet werden soll, sind diejenigen, die durch die entsprechende Bewertung der beruflichen Risiken auf der Grundlage der durchgeführten Aufgaben, der Merkmale des Arbeitsplatzes, der verwendeten Ausrüstung und Maschinen sowie der Merkmale des Arbeitnehmers bestimmt werden.',
    },
    STEGENSEK: {
      INFO: 'Die <strong>Stegensek-tabelle</strong>, ein lebendes Dokument, das von der <strong>IPTechnology-Organisation</strong> ständig aktualisiert wird, ist eine elementare und organisierte liste der einzelnen Protektoren, <strong>IP</strong>, aus der abkürzung auf englisch <strong>Individual Protector</strong> der auf dem markt vorhandenen, die ursprünglich nach <strong>Familien</strong> kategorisiert wurden, basierend auf dem gebiet des körpers, den sie schützen, oder ihrer allgemeinen typologie; und später durch <strong>Tags</strong>, die die besonderen merkmale angeben, die einige <strong>IP</strong> von anderen innerhalb jeder der <strong>Familien</strong> unterscheiden. Diese eigenschaften können sich auf strukturelle, des designs, konstruktive, funktionale oder behördliche anforderungen beziehen.',
      FAMILY: 'Familie',
      TAG: 'Etikkete',
      IP: 'IP',
      SEARCH_FAMILY: 'Familie finden ...',
      FAMILY_NOT_FOUND: 'Familie nicht gefunden ',
      SEARCH_TAG: 'Etikkete finden...',
      TAG_NOT_FOUND: 'Etikkete nicht gefunden',
      IP_LIST: 'Liste von IPs',
      EMPTY_LIST:
        'Unter dieser Familie und diesem Tag wurden keine IPs gefunden.',
      REGULATORY_FRAMEWORK: 'Gesetzlicher Rahmen',
      REGULATORY_FRAMEWORK_LIST: {
        EUROPE: 'Europa',
      },
    },
    AGENTS: {
      COUNTRY: 'País',
      COMUNITY: 'Comunidad Autónoma',
      CITY: 'Ciudad',
    },
    PAGINATOR: {
      PREVIOUS: 'Bisherige ',
      NEXT: 'Nächster',
    },
  },
};
