import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { BaseResponse } from 'src/app/shared/models/base-response';
import { environment } from 'src/environments/environment';

export interface FakeLogin {
  accessCode: string;
}
@Injectable({
  providedIn: 'root'
})
export class FakeLoginService {
  logged = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) { }

  login(code: string): Observable<BaseResponse<boolean>> {
    const fakeLogin: FakeLogin = { accessCode: code };
    return this.http.post<BaseResponse<boolean>>(`${environment.api.root}iptechnology/login`, fakeLogin);
  }

}
