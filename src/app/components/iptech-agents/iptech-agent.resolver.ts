import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { Agent } from 'src/app/shared/models/agent';
import { AgentsService } from './agents.service';

@Injectable({
  providedIn: 'root',
})
export class IptechAgentResolver implements Resolve<Agent> {
  constructor(private agentsService: AgentsService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Agent> {
    const id = route.params.id;
    if (id && id > 0) {
      return this.agentsService.getById(id);
    } else {
      return of(null);
    }
  }
}
