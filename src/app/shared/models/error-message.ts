export class ErrorMessage {
    public code?: string;
    public priority?: string;
    public fieldName?: string;
    public message?: string;
    public reason?: string;
    public solutions?: string[];
}
