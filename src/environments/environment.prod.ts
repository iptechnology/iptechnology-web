export const environment = {
  production: true,
  api: {
    root: 'https://api.iptechnology.org/',
    // root: 'http://demos-bdepi.lrcsystem.com:8083/',
    family: 'families',
    tag: 'tags',
    ips: 'ips/scan',
    agents: 'agents',
  }
};
