import { ErrorMessage } from './error-message';

export class BaseResponse<T> {
    public result?: T;
    public messages?: ErrorMessage;
}
