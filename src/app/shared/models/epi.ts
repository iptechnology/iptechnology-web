import { Base } from './base';
import { Family } from './family';
import { Image } from './image';
import { Tag } from './tag';

export class Epi extends Base {
    public code?: string;
    public name?: string;
    public descriptionIP?: string;
    public family?: Family;
    public familyId?: number;
    public reference?: string;
    public description?: string;
    public utility?: string;
    public materials?: string;
    public web?: string;
    public images?: Image[];
    public imageId?: number;
    public tag?: Tag;
    public tagId?: number;
    public enabled?: boolean;
}
