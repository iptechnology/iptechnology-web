import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { IptechAgentDetailComponent } from './iptech-agents/components/iptech-agent-detail/iptech-agent-detail.component';
import { IptechAgentResolver } from './iptech-agents/iptech-agent.resolver';
import { IptechAgentsComponent } from './iptech-agents/iptech-agents.component';
import { StegensekTablesComponent } from './stegensek-tables/stegensek-tables.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home',
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'stegensek-tables',
    component: StegensekTablesComponent,
  },
  {
    path: 'iptech-agents',
    component: IptechAgentsComponent,
  },
  {
    path: 'iptech-agents/:id',
    component: IptechAgentDetailComponent,
    resolve: { agent: IptechAgentResolver },
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'home',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComponentsRoutingModule {}
