import { SafeUrl } from '@angular/platform-browser';
import { Base } from './base';

export class Image extends Base {
    public file?: File;
    public url?: string;
    public qrUrl?: string;
    public name?: string;
    public filename?: string;
    public filenameOrigin?: string;
    public size?: number;
    public blob?: Blob;
    public qrBlob?: Blob;
    public safeUrl?: SafeUrl;
    public qrSafeUrl?: SafeUrl;
    public position?: number;
    public src?: SafeUrl;
    public showQR?: boolean = false;
}
