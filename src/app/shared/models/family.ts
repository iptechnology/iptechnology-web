import { Base } from './base';
import { Tag } from './tag';

export class Family extends Base {
    public name?: string;
    public enabled?: boolean;
    public tags?: Tag[];
    public code?: string;
}
