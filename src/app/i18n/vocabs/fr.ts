export const locale = {
  lang: 'fr',
  data: {
    MENU: {
      MENU: 'Menu',
      HOME: 'Début',
      STEGENSEK: 'Tableau Stegensek',
      IPTECH_AGENTS: 'Agents IPTech',
    },
    HOME: {
      MAIN_TEXT_1:
        "Vous entrez dans un projet d'entreprise en phase de développement naissant, parrainé par un grand groupe de professionnels spécialisés dans diverses disciplines techniques, motivé par un objectif commun: «<i> <u> <strong> Réduire le taux d'accidents du travail </strong> </u>, garantissant la conformité réglementaire et législative concernant les <strong> équipements de protection individuelle, EPI </strong> (ci-après <strong> IP </strong>) quel que soit le pays dans lequel ils sont fabriqués, distribués, nécessaires et/ou utilisé».</i>",
      MAIN_TEXT_2:
        "L'équipe <strong> IPTechnology </strong> tient à vous remercier d'avoir aidé, sur une base volontaire, à déboguer les erreurs qui, comme dans toute autre application informatique en phase de lancement, doivent être corrigées. Vous contribuez non seulement à cet objectif louable que nous vous proposons, mais vous pourrez également, à travers la personne qui vous a invité à participer, être au courant de nos succès commerciaux et de notre internationalisation et, pourquoi pas, compter sur vous dans un certain temps. , rejoindre soit en tant que professionnel indépendant, petit investisseur, soit simplement avoir accès à nos outils gratuitement.",
      MAIN_TEXT_3:
        "<strong> IPTechnology </strong> applique les technologies de l'information et de la communication (TIC) au monde de la propriété intellectuelle, développant des applications informatiques autour de la technologie spécifique appelée <strong> efpIP </strong> qui facilite leur recherche, leur sélection, leur choix et leur traçabilité . IPTechnology s'engage à maintenir une <strong> position neutre et équidistante </strong> vis-à-vis des différents agents qui composent actuellement le marché de la PI.",
      IP_RESEARCH: 'IPResearch',
      DOWNLOAD: 'Téléchargez notre application:',
      IPT_R_TEXT:
        "L'utilisation, l'entretien et la conservation des EPI doivent être effectués conformément aux instructions du fabricant. Les EPI à utiliser dans chaque poste correspondant à une organisation seront ceux déterminés par son évaluation correspondante des risques professionnels en fonction des tâches effectuées, des caractéristiques du lieu de travail, des équipements et machines utilisés et des caractéristiques du travailleur. ",
    },
    STEGENSEK: {
      INFO: "La <strong>table Stegensek</strong>, document évolutif constamment mis à jour par l'organisation <strong>IPTechnology</strong>, est une liste élémentaire et organisée de Protecteurs Individuels, <strong>IP</strong>, de l´abréviation en anglais <strong>Individual Protector</strong>, existants sur le marché,  initialement classés par <strong>Familles</strong>, en fonction de la zone du corps qu'ils protègent ou de leur typologie générale; et ensuite, par des <strong>Étiquettes</strong> qui spécifient les caractéristiques particulières qui distinguent certaines <strong>IP</strong> des autres, au sein de chacune des <strong>Familles</strong>. Ces caractéristiques peuvent faire référence à des exigences structurelles, de design, de construction, fonctionnelles ou réglementaires.",
      FAMILY: 'Familly',
      TAG: 'Étiquette',
      IP: 'IP',
      SEARCH_FAMILY: 'Trouver la familly...',
      FAMILY_NOT_FOUND: 'Familly introuvable',
      SEARCH_TAG: 'Trouver étiquette...',
      TAG_NOT_FOUND: 'Étiquette introuvable',
      IP_LIST: 'Liste IPs',
      EMPTY_LIST:
        'Aucune adresse IP trouvée sous cette famille et cette balise.',
      REGULATORY_FRAMEWORK_LIST: 'Cadre réglementaire',
      LIST: {
        EUROPE: 'Europa',
      },
    },
    AGENTS: {
      COUNTRY: 'País',
      COMUNITY: 'Comunidad Autónoma',
      CITY: 'Ciudad',
    },
    PAGINATOR: {
      PREVIOUS: 'Précédente',
      NEXT: 'Suivante',
    },
  },
};
