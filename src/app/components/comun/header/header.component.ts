import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { Agent } from 'src/app/shared/models/agent';
import { AgentsService } from '../../iptech-agents/agents.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  isLoadingAgents = true;
  agents: Agent[] = [];

  private subscriptions$ = new Subscription();

  constructor(
    private translate: TranslateService,
    private agentsService: AgentsService
  ) {}

  ngOnDestroy() {
    this.subscriptions$?.unsubscribe();
  }

  ngOnInit() {
    this.subscriptions$.add(
      this.agentsService.isLoadingList$.subscribe(
        (loading: boolean) => (this.isLoadingAgents = loading)
      )
    );

    this.subscriptions$.add(
      this.agentsService.subjectList$.subscribe(
        (agentsSubject: Agent[]) => (this.agents = agentsSubject)
      )
    );

    this.agentsService.get();
  }
}
