import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslationService } from './i18n/translation.service';
// language list
import { locale as enLang } from './i18n/vocabs/en';
import { locale as esLang } from './i18n/vocabs/es';
import { locale as frLang } from './i18n/vocabs/fr';
import { locale as itLang } from './i18n/vocabs/it';
import { locale as deLang } from './i18n/vocabs/de';
import { Subscription } from 'rxjs';
import { FakeLoginService } from './components/fake-login/fake-login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'IPTechnology';
  logged = false;

  private subscriptions$ = new Subscription();

  constructor(
    private translationService: TranslationService,
    private router: Router,
    private fakeLoginService: FakeLoginService
  ) {
    // register translations
    this.translationService.loadTranslations(
      enLang,
      esLang,
      frLang,
      itLang,
      deLang
    );
  }

  ngOnInit(): void {
    this.subscriptions$.add(
      this.fakeLoginService.logged.subscribe(logged => this.logged = logged)
    );

    // this.translationService.setLanguage('es');
  }

  ngOnDestroy(): void {
    if (this.subscriptions$) {
      this.subscriptions$.unsubscribe();
    }
  }
}
