import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AgentsService } from './agents.service';

@Component({
  selector: 'app-iptech-agents',
  templateUrl: './iptech-agents.component.html',
  styleUrls: ['./iptech-agents.component.scss'],
})
export class IptechAgentsComponent implements OnInit, OnDestroy {
  formGroup: FormGroup;

  private subscriptions$ = new Subscription();

  constructor(private agentsService: AgentsService, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.loadForm();
  }

  ngOnDestroy(): void {
    this.subscriptions$?.unsubscribe();
  }

  private loadForm(): void {
    this.formGroup = this.fb.group({
      country: new FormControl(null),
      comunity: new FormControl({ value: null, disabled: true }),
      city: new FormControl({ value: null, disabled: true }),
    });

    this.formGroup.get('country').valueChanges.subscribe((value: number) => {
      this.changeDisabledFields(true);
    });

    this.formGroup.get('comunity').valueChanges.subscribe((value: number) => {
      this.changeDisabledFields(false);
    });

    this.formGroup.get('city').valueChanges.subscribe((value: number) => {
      if (value && value > 0) {
        // this.agentsService.getByParameters();
      }
    });
  }

  private changeDisabledFields(isCountry: boolean): void {
    if (isCountry) {
      this.formGroup.get('comunity').enable();
      this.formGroup.patchValue({
        comunity: null,
        city: null,
      });
    } else {
      this.formGroup.patchValue({
        city: null,
      });
    }

    this.formGroup.get('city').disable();
  }
}
