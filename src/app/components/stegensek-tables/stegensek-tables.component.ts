import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatOptionSelectionChange } from '@angular/material/core';
import { DomSanitizer } from '@angular/platform-browser';
import { EMPTY, from, Subscription } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { Epi } from 'src/app/shared/models/epi';
import { Family } from 'src/app/shared/models/family';
import { Ip } from 'src/app/shared/models/ip';
import { Login } from 'src/app/shared/models/login';
import { Tag } from 'src/app/shared/models/tag';
import { StegensekTablesService } from './stegensek-tables.service';

@Component({
  selector: 'app-stegensek-tables',
  templateUrl: './stegensek-tables.component.html',
  styleUrls: ['./stegensek-tables.component.scss']
})
export class StegensekTablesComponent implements OnInit, OnDestroy {
  @ViewChild('familySearchInput') familySearchInput: ElementRef;
  @ViewChild('tagSearchInput') tagSearchInput: ElementRef;
  isLoadingFamilies = true;
  isLoadingTags = true;
  isLoadingIps = true;
  families: Family[] = [];
  searchFamilyList: Family[] = [];
  searchTagList: Tag[] = [];
  familySelected: Family;
  tagSelected: Tag;
  filteredEpis: Ip[] = [];
  filteredEpisToShow: Ip[] = [];
  itemsPerPage = 20;
  pages: number[] = [];
  actualPage: number;
  private epis: Ip[] = [];
  private token: string;
  private subscriptions$ = new Subscription();
  noImageUrl = './assets/img/no-photo.jpg';

  constructor(
    private http: HttpClient,
    private stegensekTablesService: StegensekTablesService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnDestroy(): void {
    this.subscriptions$?.unsubscribe();
  }

  ngOnInit(): void {
    // this.subscriptions$.add(
    //   this.http.post<Login>(`http://localhost:8083/token/authenticate`, {
    //     username: 'victor.martinez',
    //     password: '610656686'
    //   }).subscribe(authResponse => {
    //     this.token = authResponse.token;
    //     this.stegensekTablesService.getFamilies(this.token);
    //     // this.stegensekTablesService.getEPIs(this.token);
    //     // setTimeout(() => this.stegensekTablesService.getEPIs(this.token), 500);
    //   })
    // );

    this.subscriptions$.add(
      this.stegensekTablesService.isLoadingFamilyList$.subscribe(loading => {
        this.isLoadingFamilies = loading;
      })
    );

    this.subscriptions$.add(
      this.stegensekTablesService.isLoadingTagList$.subscribe(loading => {
        this.isLoadingTags = loading;
      })
    );

    this.subscriptions$.add(
      this.stegensekTablesService.isLoadingIpList$.subscribe((loading: boolean) =>
        this.isLoadingIps = loading
      )
    );

    this.subscriptions$.add(
      this.stegensekTablesService.familySubjectList$.subscribe(families => {
        this.families = families;
        this.searchFamilyList = this.families;
      })
    );

    this.subscriptions$.add(
      this.stegensekTablesService.tagSubjectList$.subscribe(tags => {
        if (this.familySelected) {
          this.familySelected.tags = tags;
          this.searchTagList = tags;
        }
      })
    );

    this.subscriptions$.add(
      this.stegensekTablesService.ipSubjectList$.subscribe(epis => {
        this.epis = epis;
        this.filterEpis();
      })
    );

    this.stegensekTablesService.getFamilies();
    // setTimeout(() => this.stegensekTablesService.getEPIs(), 500);
  }

  selectFamily(event: MatOptionSelectionChange): void {
    if (event.isUserInput) {
      const idFamily = +event.source.value;
      this.familySelected = this.families.find(f => f.id === idFamily);
      if (idFamily !== -1) {
        this.stegensekTablesService.getTagsOfFamily(idFamily);
      }
      this.familySearchInput.nativeElement.value = null;
      this.searchFamilyList = this.families;
      this.tagSelected = null;
      this.filteredEpisToShow = [];
    }
  }

  selectTag(event: MatOptionSelectionChange): void {
    if (event.isUserInput) {
      const idTag = +event.source.value;
      this.tagSelected = this.familySelected.tags.find(t => t.id === idTag);
      this.tagSearchInput.nativeElement.value = null;
      this.searchTagList = this.familySelected.tags;
      this.stegensekTablesService.getIPs(this.familySelected.id, this.tagSelected.id);
    }
  }

  onSearchFamily(value: string): void {
    this.searchFamilyList = this.families.filter(family => {
      return `${family.code} - ${family.name}`.toLowerCase().includes(value.toLowerCase());
    });
  }

  onSearchTag(value: string): void {
    this.searchTagList = this.familySelected.tags.filter(tag => {
      return `${tag.code} - ${tag.name}`.toLowerCase().includes(value.toLowerCase()) || tag.position.toString().includes(value);
    });
  }

  filterEpis(): void {
    // this.filteredEpis = this.epis.filter(epi => epi.familyId === this.familySelected.id && epi.tagId === this.tagSelected.id);
    this.filteredEpis = this.epis;
    const pages = this.filteredEpis?.length < this.itemsPerPage ? 1 : this.filteredEpis?.length / this.itemsPerPage || 0;
    this.pages = [];
    for (let i = 0; i < pages; i++) {
      this.pages.push(i);
    }
    this.pagging(0);
    // this.getImages();
  }

  // getImages(): void {
  //   if (this.filteredEpisToShow.find(pa => pa.images && pa.images.length > 0 && !pa.images[0].safeUrl)) {
  //     const pageArrayWithoutImages = this.filteredEpisToShow.filter(pa => pa.images && pa.images.length > 0 && !pa.images[0].safeUrl);
  //     from(pageArrayWithoutImages).pipe(
  //       concatMap(epi => {
  //         if (epi.images) {
  //           return this.stegensekTablesService.downloadImageEPI(epi.images[0].id, epi.id);
  //         } else {
  //           return EMPTY;
  //         }
  //       })
  //     ).subscribe(imageResponse => {
  //       const idEPI = imageResponse.keys().next().value;
  //       const blob = imageResponse.get(idEPI);
  //       const objectURL = URL.createObjectURL(blob);
  //       const imageEPI = pageArrayWithoutImages.find(op => op.id === idEPI).images[0];
  //       imageEPI.blob = blob;
  //       imageEPI.url = URL.createObjectURL(blob);
  //       imageEPI.safeUrl = this.sanitizer.bypassSecurityTrustUrl(objectURL);
  //       imageEPI.src = imageEPI.safeUrl;
  //       imageEPI.qrSafeUrl = this.sanitizer.bypassSecurityTrustUrl('./assets/img/qr.png');
  //     });
  //   }
  // }

  pagging(page: number): void {
    this.actualPage = page;
    this.filteredEpisToShow = this.filteredEpis?.slice(page * this.itemsPerPage, (page + 1) * this.itemsPerPage) || [];
  }

}
