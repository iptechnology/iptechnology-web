import { Base } from './base';

export class Agent extends Base {
    public name?: string;
    public surname1?: string;
    public surname2?: string;
    public fullname?: string;
    public address1?: string;
    public address2?: string;
    public postcode?: string;
    public town?: string;
    public locality?: string;
    public prefix1?: string;
    public phone1?: string;
    public prefix2?: string;
    public phone2?: string;
    public email?: string;
    public dateCreate?: string;
    public dateCreateDate?: Date;
    public web?: string;
    public enabled?: boolean;
}
