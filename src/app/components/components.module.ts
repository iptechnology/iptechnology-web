import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './comun/header/header.component';
import { FooterComponent } from './comun/footer/footer.component';
import { StegensekTablesComponent } from './stegensek-tables/stegensek-tables.component';
import { ComponentsRoutingModule } from './components-routing.module';
import { SharedModule } from '../shared/shared.module';
import { HomeComponent } from './home/home.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslationModule } from '../i18n/translation.module';
import { LanguageSelectorComponent } from './comun/header/components/language-selector/language-selector.component';
import { FakeLoginComponent } from './fake-login/fake-login.component';
import { IptechAgentsComponent } from './iptech-agents/iptech-agents.component';
// import { TranslateHttpLoader } from '@ngx-translate/http-loader';

@NgModule({
  imports: [
    CommonModule,
    ComponentsRoutingModule,
    SharedModule,
    TranslationModule,
    // TranslateModule.forRoot(),
    // TranslateModule.forRoot({
    //   loader: {
    //     provide: TranslateLoader,
    //     useFactory: HttpLoaderFactory,
    //     deps: [HttpClient]
    //   }
    // }),
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    StegensekTablesComponent,
    HomeComponent,
    LanguageSelectorComponent,
    FakeLoginComponent,
    IptechAgentsComponent,
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    StegensekTablesComponent,
    HomeComponent,
    LanguageSelectorComponent,
    FakeLoginComponent,
  ],
})
export class ComponentsModule {}

// required for AOT compilation
// export function HttpLoaderFactory(http: HttpClient) {
//   return new TranslateHttpLoader(http);
// }
