import { Base } from './base';

export class Ip extends Base {
    public image?: Blob;
    public qrElemental?: Blob;
    public imageToShow?: Blob;
}
