import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { TranslationService } from 'src/app/i18n/translation.service';
import { Agent } from 'src/app/shared/models/agent';
import { BaseResponse } from 'src/app/shared/models/base-response';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AgentsService {
  subjectList$ = new Subject<Agent[]>();
  isLoadingList$ = new Subject<boolean>();
  language: string;

  // tslint:disable-next-line: variable-name
  private _dataList: Agent[];
  public get dataList(): Agent[] {
    return this._dataList;
  }
  public set dataList(v: Agent[]) {
    this._dataList = v;
  }

  constructor(
    private http: HttpClient,
    private translationService: TranslationService
  ) {
    this.language = translationService.getSelectedLanguage();
  }

  get(): void {
    this.isLoadingList$.next(true);
    if (this.dataList) {
      this.subjectList$.next([...this.dataList]);
      this.isLoadingList$.next(false);
    } else {
      const headers = new HttpHeaders({
        'Accept-Language': this.language,
        'Content-Type': 'application/json',
        // Authorization: token
      });
      this.http
        .get<BaseResponse<Agent[]>>(
          `${environment.api.root}${environment.api.agents}?enabled=true`,
          { headers }
        )
        .subscribe((familiesResponse: BaseResponse<Agent[]>) => {
          if (familiesResponse.result) {
            this.dataList = familiesResponse.result;
            this.subjectList$.next([...this.dataList]);
          }
        });
    }
  }

  getById(id: number): Observable<Agent> {
    return this.http
      .get<BaseResponse<Agent>>(
        `${environment.api.root}${environment.api.agents}/${id}`
      )
      .pipe(map((responseData: BaseResponse<Agent>) => responseData.result));
  }

  getByParameters(): void {}
}
