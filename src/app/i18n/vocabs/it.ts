export const locale = {
  lang: 'it',
  data: {
    MENU: {
      MENU: 'Menù',
      HOME: 'Inizio',
      STEGENSEK: 'Tavolo Stegensek',
      IPTECH_AGENTS: 'Agenti IPTech',
    },
    HOME: {
      MAIN_TEXT_1:
        'Stai entrando in un progetto imprenditoriale in una fase di sviluppo incipiente, sponsorizzato da un nutrito gruppo di professionisti specializzati in varie discipline tecniche, motivati ​​da un obiettivo comune: "<i> <u> <strong> Ridurre il tasso di infortuni sul lavoro </strong> </u>, garantendo la conformità normativa e legislativa in merito a <strong> Dispositivi di protezione individuale, DPI </strong> (di seguito <strong> IP </strong>) indipendentemente dal paese in cui sono prodotti, distribuiti, necessari e/o usato".</i> ',
      MAIN_TEXT_2:
        "Il team di <strong> IPTechnology </strong> desidera ringraziarti per aver aiutato, su base volontaria, a eseguire il debug degli errori che, come in qualsiasi altra applicazione informatica in fase di lancio, devono essere corretti. Non solo stai contribuendo a quel lodevole obiettivo che ti proponiamo, ma sarai anche in grado, attraverso la persona che ti ha invitato a partecipare, di essere consapevole dei nostri successi aziendali e dell'internazionalizzazione e, perché no, di contare su di te tra qualche tempo , aderendo come professionista indipendente, piccolo investitore o semplicemente avendo accesso ai nostri strumenti gratuitamente.",
      MAIN_TEXT_3:
        "<strong> IPTechnology </strong> applica le tecnologie dell'informazione e della comunicazione (ICT) al mondo della PI, sviluppando applicazioni informatiche attorno alla specifica tecnologia nota come <strong> efpIP </strong> che ne facilita la ricerca, la selezione, la scelta e la tracciabilità . IPTechnology si impegna a mantenere una <strong> posizione neutra ed equidistante </strong> rispetto ai diversi agenti che attualmente compongono il mercato della PI.",
      IP_RESEARCH: 'IPResearch',
      DOWNLOAD: 'Scarica la nostra applicazione:',
      IPT_R_TEXT:
        "L'uso, la manutenzione e la conservazione dei DPI devono essere effettuati in conformità alle istruzioni del produttore. I DPI da utilizzare in ogni mansione corrispondente ad un'organizzazione saranno quelli determinati dalla sua corrispondente valutazione dei rischi professionali sulla base delle mansioni svolte, delle caratteristiche del luogo di lavoro, delle attrezzature e delle macchine utilizzate e delle caratteristiche del lavoratore. ",
    },
    STEGENSEK: {
      INFO: "La <strong>tavola Stegensek</strong>, un documento vivo e costantemente aggiornato dall'organizzazione <strong>IPTechnology</strong>, è un elenco elementare e organizzato di Protettori Individuali, IP, dalla sua abbreviazione in inglese <strong>Individual Protector</strong>, di quelli esistenti sul mercato, categorizzati inizialmente per <strong>Famiglie</strong>, secondo la zona del corpo che proteggono o la loro tipologia generale; e successivamente, per <strong>Tags/Etichette</strong>, che specificano le caratteristiche particolari che distinguono alcuni <strong>IP</strong> da altri, dentro di ciascuna delle <strong>Famiglie</strong>. Queste caratteristiche possono riferirsi a questioni di requisiti strutturali, de designo, costruttivi, funzionali o requisiti normativi.",
      FAMILY: 'Famiglia',
      TAG: 'Etichette',
      IP: 'IP',
      SEARCH_FAMILY: 'Trova famiglia...',
      FAMILY_NOT_FOUND: 'Famiglia non trovata',
      SEARCH_TAG: 'Trova etichette...',
      TAG_NOT_FOUND: 'Etichette non trovata',
      IP_LIST: 'Elenco delle IP',
      EMPTY_LIST: 'Nessun IP trovato sotto questa famiglia e questo tag.',
      REGULATORY_FRAMEWORK: 'Quadro normativo ',
      REGULATORY_FRAMEWORK_LIST: {
        EUROPE: 'Europa',
      },
    },
    AGENTS: {
      COUNTRY: 'País',
      COMUNITY: 'Comunidad Autónoma',
      CITY: 'Ciudad',
    },
    PAGINATOR: {
      PREVIOUS: 'Precedente',
      NEXT: 'Il prossimo',
    },
  },
};
