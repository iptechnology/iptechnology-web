import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Epi } from 'src/app/shared/models/epi';
import { Family } from 'src/app/shared/models/family';
import { Tag } from 'src/app/shared/models/tag';
import { map } from 'rxjs/operators';
import { TranslationService } from 'src/app/i18n/translation.service';
import { environment } from 'src/environments/environment';
import { Ip } from 'src/app/shared/models/ip';
import { BaseResponse } from 'src/app/shared/models/base-response';

@Injectable({
  providedIn: 'root'
})
export class StegensekTablesService {
  familySubjectList$ = new Subject<Family[]>();
  tagSubjectList$ = new Subject<Tag[]>();
  ipSubjectList$ = new Subject<Epi[]>();
  isLoadingFamilyList$ = new Subject<boolean>();
  isLoadingTagList$ = new Subject<boolean>();
  isLoadingIpList$ = new Subject<boolean>();
  language: string;

  // tslint:disable-next-line: variable-name
  private _dataList: Family[];
  public get dataList(): Family[] {
    return this._dataList;
  }
  public set dataList(v: Family[]) {
    this._dataList = v;
  }

  constructor(
    private http: HttpClient,
    private translationService: TranslationService
  ) {
    this.language = translationService.getSelectedLanguage();
  }

  getFamilies(): void {
    this.isLoadingFamilyList$.next(true);
    if (this.dataList) {
      this.familySubjectList$.next([...this.dataList]);
      this.isLoadingFamilyList$.next(false);
    } else {
      const headers = new HttpHeaders({
        'Accept-Language': this.language,
        'Content-Type': 'application/json',
        // Authorization: token
      });
      this.http.get<BaseResponse<Family[]>>(`${environment.api.root}${environment.api.family}?enabled=true`, { headers })
        .subscribe((familiesResponse: BaseResponse<Family[]>) => {
          if (familiesResponse.result) {
            this.dataList = familiesResponse.result;
            this.familySubjectList$.next([...this.dataList]);
          }
        });
    }
  }

  getTagsOfFamily(familyId: number): void {
    this.isLoadingTagList$.next(true);
    const family = this.dataList?.find(f => f.id === familyId);
    if (family?.tags?.length > 0) {
      this.tagSubjectList$.next([...family.tags]);
      this.isLoadingTagList$.next(false);
    } else {
      const headers = new HttpHeaders({
        'Accept-Language': this.language,
        'Content-Type': 'application/json',
        // Authorization: token
      });
      this.http.get<BaseResponse<Tag[]>>(`${environment.api.root}${environment.api.tag}?familyId=${familyId}&enabled=true`, { headers })
        .subscribe((tagsResponse: BaseResponse<Tag[]>) => {
          if (family) {
            if (tagsResponse.result) {
              this.dataList.find(f => f.id === familyId).tags = tagsResponse.result;
              family.tags = tagsResponse.result;
              family.tags.sort(this.sortTags);
              this.tagSubjectList$.next([...family.tags]);
            }
          } else {
            if (tagsResponse.result) {
              const tags = tagsResponse.result;
              tags.sort(this.sortTags);
              this.tagSubjectList$.next([...tags]);
            }
          }
          this.isLoadingTagList$.next(false);
        });
    }
  }

  private sortTags(tagA: Tag, tagB: Tag): number {
    return tagA.position - tagB.position;
  }


  getIPs(familyId: number, tagId: number): void {
    this.isLoadingIpList$.next(true);
    const tag = this.dataList.find(family => family.id === familyId).tags.find(t => t.id === tagId);
    if (tag?.ips?.length > 0) {
      this.ipSubjectList$.next([...tag.ips]);
      this.isLoadingIpList$.next(false);
    } else {
      const headers = new HttpHeaders({
        'Accept-Language': this.language,
        'Content-Type': 'application/json',
        // Authorization: token
      });
      this.http.get<BaseResponse<Ip[]>>(`${environment.api.root}${environment.api.ips}?familyId=${familyId}&tagId=${tagId}&linked=true&enabled=true`,
        { headers })
        .pipe(
          map((ipsResponse: BaseResponse<Ip[]>) => {
            ipsResponse.result?.forEach(ip => {
              if (ip.image) {
                ip.imageToShow = ip.image;
              }
            });
            return ipsResponse;
          })
        )
        .subscribe((ipsResponse: BaseResponse<Ip[]>) => {
          if (ipsResponse.result) {
            if (tag) {
              tag.ips = ipsResponse.result;
            }
            this.ipSubjectList$.next(ipsResponse.result);
          }
          this.isLoadingIpList$.next(false);
        });
    }
  }

}
