export const locale = {
  lang: 'es',
  data: {
    MENU: {
      MENU: 'Menú',
      HOME: 'Inicio',
      STEGENSEK: 'Tabla Stegensek',
      IPTECH_AGENTS: 'Agentes IPTech',
    },
    HOME: {
      MAIN_TEXT_1:
        'Está usted entrando en un proyecto empresarial en fase de incipiente desarrollo, auspiciado por un amplio grupo de profesionales especializados en varias disciplinas técnicas, motivados por un objetivo común: “<i><u><strong>Reducir la siniestralidad laboral</strong></u>, asegurando el cumplimiento normativo y legislativo en lo concerniente a los <strong>Equipos de Protección Individual, EPI</strong> (en adelante <strong>IP</strong>) independientemente del país en que se fabriquen, se distribuyan, se necesiten y/o se usen”.</i>',
      MAIN_TEXT_2:
        'El equipo humano de <strong>IPTechnology</strong> quiere darle las gracias por ayudar, de manera voluntaria, a depurar los errores que, como en cualquier otra aplicación informática en fase de lanzamiento, deban ser corregidos. Usted no sólo está contribuyendo con ese objetivo loable que nos proponemos, sino que también podrá, a través de la persona que le ha invitado a participar, estar al corriente de nuestros éxitos empresariales e internacionalización y, por qué no, a contar con usted en algún momento, incorporándose ya sea como profesional independiente, pequeño inversor, o simplemente teniendo acceso a nuestras herramientas de manera gratuita.',
      MAIN_TEXT_3:
        '<strong>IPTechnology</strong> aplica las Tecnologías de Información y Comunicaciones (TIC) al mundo del IP, desarrollando aplicaciones informáticas en torno a la tecnología específica conocida como <strong>efpIP</strong> que facilita su búsqueda, selección, elección y trazabilidad. IPTechnology se compromete a mantener una <strong>posición neutra y equidistante</strong> con respecto a los distintos agentes que en la actualidad conforman el mercado de los IP.',
      IP_RESEARCH: 'IPResearch',
      DOWNLOAD: 'Descárgate nuestra aplicación:',
      IPT_R_TEXT:
        'La utilización, mantenimiento y conservación de los EPIs debe realizarse conforme a las indicaciones del fabricante. Los EPIs a utilizar en cada puesto de trabajo correspondiente a una organización, serán los que determine su correspondiente evaluación de riesgos laborales en función de las tareas realizadas, las características del lugar de trabajo, los equipos y máquinas utilizados y las características el trabajador/a.',
    },
    STEGENSEK: {
      INFO: 'La <strong>tabla Stegensek</strong>, documento vivo actualizado constantemente por parte de la organización <strong>IPTechnology</strong>, es una lista elemental y organizada de Protectores Individuales, <strong>IP</strong>, de su abreviatura en inglés <strong>Individual Protector</strong>, de los existentes en el mercado, categorizados inicialmente por <strong>Familias</strong>, en función de la zona del cuerpo que protegen o de su tipología general; y posteriormente, por <strong>Etiquetas</strong>, que especifican las características particulares que distinguen unos <strong>IP</strong> de otros, dentro de cada una de las <strong>Familias</strong>. Dichas características pueden hacer referencia a cuestiones estructurales, de diseño, constructivas, funcionales o de requerimientos normativos.',
      FAMILY: 'Familia',
      TAG: 'Etiqueta',
      IP: 'IP',
      SEARCH_FAMILY: 'Buscar familia...',
      FAMILY_NOT_FOUND: 'No se ha encontrado la familia',
      SEARCH_TAG: 'Buscar etiqueta...',
      TAG_NOT_FOUND: 'No se ha encontrado la etiqueta',
      IP_LIST: 'Listado de IPs',
      EMPTY_LIST: 'No se han encontrado IPs bajo esta familia y etiqueta.',
      REGULATORY_FRAMEWORK: 'Marco normativo',
      REGULATORY_FRAMEWORK_LIST: {
        EUROPE: 'Europa',
      },
    },
    AGENTS: {
      COUNTRY: 'País',
      COMUNITY: 'Comunidad Autónoma',
      CITY: 'Ciudad',
    },
    PAGINATOR: {
      PREVIOUS: 'Anterior',
      NEXT: 'Siguiente',
    },
  },
};
