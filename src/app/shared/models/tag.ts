import { Base } from './base';
import { Ip } from './ip';

export class Tag extends Base {
    public name?: string;
    public enabled?: boolean;
    public position?: number;
    public ips?: Ip[];
    public code?: string;
}
