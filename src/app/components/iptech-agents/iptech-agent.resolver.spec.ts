import { TestBed } from '@angular/core/testing';

import { IptechAgentResolver } from './iptech-agent.resolver';

describe('IptechAgentResolver', () => {
  let resolver: IptechAgentResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(IptechAgentResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
